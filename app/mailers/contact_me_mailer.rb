class ContactMeMailer < ApplicationMailer
  def send_mail(contact_mail)
    @contact_mail = contact_mail
    mail(to: 'krassm88@gmail.com', subject: 'Contact me...')
  end
end
