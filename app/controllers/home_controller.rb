class HomeController < ApplicationController
  def index
  end

  def contact_me
    @contact_mail = ContactMail.new(contact_params)
    if @contact_mail.save
      ContactMeMailer.send_mail(@contact_mail).deliver_now
      true
    else
      false
    end
  end

  private

  def contact_params
    params.permit(:name, :phone, :email, :message)
  end
end