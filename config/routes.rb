Rails.application.routes.draw do
  post :contact_me, to: 'home#contact_me'
  root to: 'home#index'
end
